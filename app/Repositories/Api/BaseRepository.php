<?php

namespace App\Repositories\Api;

class BaseRepository{

    public $model;

    public function __construct($model)
    {
        $this->model = $model;
    }
    public function index($name, $offset, $limit)
    {
        return $this->model->where('name', 'like', '%'.$name.'%')
                    ->offset($offset)
                    ->limit($limit)
                    ->get();
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function update($model , array $data)
    {
        return $model->update($data);
    }

    public function destroy($model)
    {
        return $model->delete();
    }

    public function count()
    {
        return $this->model->count();
    }

    public function find($id)
    {
        return $this->model::find($id);
    }
}
