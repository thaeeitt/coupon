<?php
namespace App\Repositories\Api;

use App\Coupon;

class CouponRepository extends BaseRepository{

    public $model;
    public function __construct(Coupon $coupon)
    {
        parent::__construct($coupon);
        $this->model = $coupon;
    }

}