<?php
namespace App\Repositories\Api;

use App\Shop;

class ShopRepository extends BaseRepository{

    public $model;
    public function __construct(Shop $shop)
    {
        parent::__construct($shop);
        $this->model = $shop;
    }

}