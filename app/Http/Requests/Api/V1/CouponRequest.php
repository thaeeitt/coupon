<?php

namespace App\Http\Requests\Api\V1;

use App\Services\FormatErrors;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:128',
            'description' => 'nullable',
            'discount_type' => 'required|in:percentage,fix-amount',
            'amount' => 'required|integer',
            'image_url' => 'nullable',
            'code' => 'required|integer',
            'start_datetime' => 'nullable|date_format:Y-m-d H:i:s',
            'end_datetime' => 'nullable|date_format:Y-m-d H:i:s|after:start_datetime',
            'coupon_type' => 'required|in:private,public',
            'used_count' => 'required|integer',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $start = microtime(true);
        $response_data = config('responsedata');
        $response_data['success'] = 0;
        $response_data['code'] = 400;
        $response_data['meta'] = [
            "method" => Request::method(),
            "path" => Request::path()
        ];
        $response_data['errors'] = [
            "message" => "The request parameters are incorrect, please make sure to follow the documentation about request parameters of the resource.",
            "code" => 400002,
            "validation" => FormatErrors::format($validator)
        ];
        $response_data['duration'] = number_format(microtime(true) - $start, 3);
        throw new HttpResponseException(response()->json($response_data, Response::HTTP_UNPROCESSABLE_ENTITY));
    }

}
