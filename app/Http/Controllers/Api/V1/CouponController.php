<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\CouponRequest;
use App\Http\Resources\CouponCollection;
use App\Http\Resources\CouponResource;
use App\Repositories\Api\CouponRepository;
use Symfony\Component\HttpFoundation\Response;

class CouponController extends Controller
{
    public $method, $endpoint, $response_data;

    public function __construct(Request $request)
    {
        $this->method = $request->method();
        $this->endpoint = $request->path();
        $this->response_data = config('responsedata');
    }

    public function getCoupons(Request $request, CouponRepository $coupon_repo)
    {
        $start = microtime(true);
        if(isset($request->name)) {
            //retrieve query params
            $name = $request->name;
            $limit = isset($request->limit) ? (int)($request->limit) : 30;
            $offset = isset($request->offset) ? (int)($request->offset) : 0;
            //query coupon filter by name
            $coupons = $coupon_repo->index($name, $offset, $limit);
            //set response data
            $this->response_data['data'] = new CouponCollection($coupons);
            $this->response_data['meta'] = [
                'method' => $this->method,
                'endpoint' => $this->endpoint,
                'limit' => $limit,
                'offset' => $offset,
                'total' => $coupons->count()
            ];
            $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
            return response()->json($this->response_data);
        }
        
    }

    public function getCouponById($id, CouponRepository $coupon_repo)
    {
        $start = microtime(true);
        $coupon = $coupon_repo->find($id);
        if($coupon){
            //set response data
            $this->response_data['data'] = new CouponResource($coupon);
        }else {
            //response error status
            $this->response_data['success'] = 0;
            $this->response_data['code'] = Response::HTTP_NOT_FOUND;
            $this->response_data['errors'] = [
                "message" => "The resource that matches the request ID does not found.",
                "code" => 404002
            ];
        }
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }

    public function createCoupon(CouponRequest $request, CouponRepository $coupon_repo)
    {
        $start = microtime(true);
        $validated_data = $request->validated();
        $validated_data['admin_id'] = 2;
        $coupon = $coupon_repo->store($validated_data);
        //set response data
        $this->response_data['code'] = 201;
        $this->response_data['data'] = [
            'id' => $coupon->id
        ];
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }

    public function updateCoupon($id, CouponRequest $request, CouponRepository $coupon_repo)
    {
        $start = microtime(true);
        if($coupon = $coupon_repo->find($id)) {
            $validated_data = $request->validated();
            $validated_data['admin_id'] = 2;
            $coupon = $coupon_repo->update($coupon, $validated_data);
            //set response data
            $this->response_data['data'] = [
                'updated' => 1
            ];
        } else {
            //response error status
            $this->response_data['success'] = 0;
            $this->response_data['code'] = Response::HTTP_NOT_FOUND;
            $this->response_data['errors'] = [
                "message" => "The updating resource that corresponds to the ID wasn't found.",
                "code" => 404003
            ];
        }
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }

    public function deleteCoupon($id, CouponRepository $coupon_repo)
    {
        $start = microtime(true);
        if($coupon = $coupon_repo->find($id)) {
            $coupon_repo->destroy($coupon);
            //set success response data
            $this->response_data['data'] = [
                'deleted' => 1
            ];
        }else {
            //response error status
            $this->response_data['success'] = 0;
            $this->response_data['code'] = Response::HTTP_NOT_FOUND;
            $this->response_data['errors'] = [
                "message" => "The deleting resource that corresponds to the ID wasn't found.",
                "code" => 404004
            ];
        }
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }
}
