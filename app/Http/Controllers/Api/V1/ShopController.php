<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ShopResource;
use App\Http\Resources\ShopCollection;
use App\Repositories\Api\ShopRepository;
use App\Http\Requests\Api\V1\ShopRequest;
use Symfony\Component\HttpFoundation\Response;

class ShopController extends Controller
{
    public $method, $endpoint, $response_data;

    public function __construct(Request $request)
    {
        $this->method = $request->method();
        $this->endpoint = $request->path();
        $this->response_data = config('responsedata');
    }

    public function getShops(Request $request, ShopRepository $shop_repo)
    {
        $start = microtime(true);
        if(isset($request->name)) {
            //retrieve query params
            $name = $request->name;
            $limit = isset($request->limit) ? (int)($request->limit) : 30;
            $offset = isset($request->offset) ? (int)($request->offset) : 0;
            //query shop filter by name
            $shops = $shop_repo->index($name, $offset, $limit);
            //set response data
            $this->response_data['data'] = new ShopCollection($shops);
            $this->response_data['meta'] = [
                'method' => $this->method,
                'endpoint' => $this->endpoint,
                'limit' => $limit,
                'offset' => $offset,
                'total' => $shops->count()
            ];
            $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
            return response()->json($this->response_data);
        }
        
    }

    public function getShopById($id, ShopRepository $shop_repo)
    {
        $start = microtime(true);
        $shop = $shop_repo->find($id);
        if($shop){
            //set response data
            $this->response_data['data'] = new ShopResource($shop);
        }else {
            //response error status
            $this->response_data['success'] = 0;
            $this->response_data['code'] = Response::HTTP_NOT_FOUND;
            $this->response_data['errors'] = [
                "message" => "The resource that matches the request ID does not found.",
                "code" => 404002
            ];
        }
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }

    public function createShop(ShopRequest $request, ShopRepository $shop_repo)
    {
        $start = microtime(true);
        $validated_data = $request->validated();
        $validated_data['admin_id'] = 2;
        $shop = $shop_repo->store($validated_data);
        //set response data
        $this->response_data['code'] = Response::HTTP_CREATED ;
        $this->response_data['data'] = [
            'id' => $shop->id
        ];
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }

    public function updateShop($id, ShopRequest $request, ShopRepository $shop_repo)
    {
        $start = microtime(true);
        if($shop = $shop_repo->find($id)) {
            $validated_data = $request->validated();
            $validated_data['admin_id'] = 2;
            $shop = $shop_repo->update($shop, $validated_data);
            //set response data
            $this->response_data['data'] = [
                'updated' => 1
            ];
        } else {
            //response error status
            $this->response_data['success'] = 0;
            $this->response_data['code'] = Response::HTTP_NOT_FOUND;
            $this->response_data['errors'] = [
                "message" => "The updating resource that corresponds to the ID wasn't found.",
                "code" => 404003
            ];
        }
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }

    public function deleteShop($id, ShopRepository $shop_repo)
    {
        $start = microtime(true);
        if($shop = $shop_repo->find($id)) {
            $shop_repo->destroy($shop);
            //set success response data
            $this->response_data['data'] = [
                'deleted' => 1
            ];
        }else {
            //response error status
            $this->response_data['success'] = 0;
            $this->response_data['code'] = Response::HTTP_NOT_FOUND;
            $this->response_data['errors'] = [
                "message" => "The deleting resource that corresponds to the ID wasn't found.",
                "code" => 404004
            ];
        }
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }
}
