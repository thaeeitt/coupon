<?php

namespace App\Http\Controllers\Api\V1;

use App\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\CouponShopResource;
use App\Repositories\Api\CouponRepository;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Api\V1\CouponShopRequest;

class CouponShopController extends Controller
{
    public $method, $endpoint, $response_data;

    public function __construct(Request $request)
    {
        $this->method = $request->method();
        $this->endpoint = $request->path();
        $this->response_data = config('responsedata');
    }

    public function getShops(Request $request, CouponRepository $coupon_repo)
    {
        $start = microtime(true);
        if (isset($request->coupon_id)) {
            //retrieve query params
            if ($coupon = $coupon_repo->find($request->coupon_id)) {
                $limit = isset($request->limit) ? (int) ($request->limit) : 30;
                $offset = isset($request->offset) ? (int) ($request->offset) : 0;
                $coupon = Coupon::find($request->coupon_id);
                $shops = $coupon->shops->slice($offset, $limit);

                //set response data
                $this->response_data['data'] = new CouponShopResource($coupon, $shops);
                $this->response_data['meta'] = [
                    'method' => $this->method,
                    'endpoint' => $this->endpoint,
                    'limit' => $limit,
                    'offset' => $offset,
                    'total' => $shops->count()
                ];
                $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
                return response()->json($this->response_data);
            }
        }
    }

    public function CouponShopById(CouponShopRequest $request)
    {
        $start = microtime(true);
        $validated_data = $request->validated();
        $coupon = Coupon::find($validated_data['coupon_id']);
        $shops = $coupon->shops->where('id', $validated_data['shop_id']);
        if ($coupon && $shops) {
            //set response data
            $this->response_data['data'] = new CouponShopResource($coupon, $shops);
        } else {
            //response error status
            $this->response_data['success'] = 0;
            $this->response_data['code'] = Response::HTTP_NOT_FOUND;
            $this->response_data['errors'] = [
                "message" => "The resource that matches the request ID does not found.",
                "code" => 404002
            ];
        }
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }



    public function deleteCouponShop(Request $request)
    {
        $start = microtime(true);
        if ($request->coupon_id && $request->id) {
            $coupon_shop = DB::table('coupon_shops')->where('id', $request->id)->where('coupon_id', $request->coupon_id)->count();
            if ($coupon_shop) {
                DB::table('coupon_shops')->where('id', $request->id)->delete();
                //set success response data
                $this->response_data['data'] = [
                    'deleted' => 1
                ];
            } else {
                //response error status
                $this->response_data['success'] = 0;
                $this->response_data['code'] = Response::HTTP_NOT_FOUND;
                $this->response_data['errors'] = [
                    "message" => "The deleting resource that corresponds to the ID wasn't found.",
                    "code" => 404004
                ];
            }
            $this->response_data['meta'] = [
                'method' => $this->method,
                'endpoint' => $this->endpoint
            ];
            $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
            return response()->json($this->response_data);
        }
    }

    public function createCouponShop(CouponShopRequest $request)
    {
        $start = microtime(true);
        $validated_data = $request->validated();
        $coupon_shop = DB::table('coupon_shops')->where('coupon_id', $validated_data['coupon_id'])->where('shop_id', $validated_data['shop_id'])->count();
        if (!$coupon_shop) {
            $coupon_shop = DB::table('coupon_shops')->insertGetId(
                ['shop_id' => $validated_data['shop_id'], 
                'coupon_id' => $validated_data['coupon_id'],
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()]
            );
            $this->response_data['code'] = Response::HTTP_CREATED;
            $this->response_data['data'] = [
                'id' => $coupon_shop
            ];
        } else {
            //response error status
            $this->response_data['success'] = 0;
            $this->response_data['code'] = 409;
            $this->response_data['errors'] = [
                "message" => "The inserting resource was already registered.",
                "code" => 409001
            ];
        }
        $this->response_data['meta'] = [
            'method' => $this->method,
            'endpoint' => $this->endpoint
        ];
        $this->response_data['duration'] = number_format(microtime(true) - $start, 3);
        return response()->json($this->response_data);
    }
}
