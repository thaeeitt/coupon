<?php

namespace App\Http\Resources;

use App\Shop;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ShopCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Shop $shop) {
            return (new ShopResource($shop));
        });
        return parent::toArray($request);
    }
}
