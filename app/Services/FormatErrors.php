<?php
namespace App\Services;
use Illuminate\Contracts\Validation\Validator;

class  FormatErrors
{
    public static function format(Validator $validator)
    {
        $errors = $validator->errors()->getMessages();
        $obj = $validator->failed();
        $validations = [];
        foreach($obj as $input => $rules){
            $i = 0;
            $errs = [];
            foreach($rules as $rule => $ruleInfo){
                
                $key = $rule;
                foreach($ruleInfo as $tag){
                    $key .= '.'.$tag;
                }
                $error = [
                    "key" => strtolower($key),
                    "message" => $errors[$input][$i]
                ];
                array_push($errs, $error);
                $i++;
            }
            $validation = [
                "attribute" => $input,
                "errors" => $errs,
            ];
            array_push($validations, $validation);
        }
        return $validations;
    }
}