<?php

    return [
        "success" => 1,
        "code" => 200,
        "meta" => null,
        "data" => [],
        "errors" => [],
        "duration" => 0 
    ];