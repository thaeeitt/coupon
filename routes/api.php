<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('Api\V1')->group(function(){
    // Login or register
    Route::get('/coupons' , 'CouponController@getCoupons');
    Route::get('/coupons/{id}', 'CouponController@getCouponById');
    Route::post('/coupons', 'CouponController@createCoupon');
    Route::put('/coupons/{id}', 'CouponController@updateCoupon');
    Route::delete('/coupons/{id}', 'CouponController@deleteCoupon');

    Route::get('/shops' , 'ShopController@getShops');
    Route::get('/shops/{id}', 'ShopController@getShopById');
    Route::post('/shops', 'ShopController@createShop');
    Route::put('/shops/{id}', 'ShopController@updateShop');
    Route::delete('/shops/{id}', 'ShopController@deleteShop');

    Route::get('/coupons/{coupon_id}/shops', 'CouponShopController@getShops');
    Route::get('/coupons/{coupon_id}/shops/{shop_id}', 'CouponShopController@CouponShopById');
    Route::post('/coupons/{coupon_id}/shops/{shop_id}', 'CouponShopController@createCouponShop');
    Route::delete('/coupons/{coupon_id}/shops/{id}', 'CouponShopController@deleteCouponShop');
});
